import React, { useContext } from 'react';
import styled from 'styled-components';

import StoreContext from './../../context/StoreContext';

import ProductCard from '../../components/ProductCard';

const ProductListContainer = ({setAppData, setProductsToShow}) => {
    const { products }:any = useContext(StoreContext);

    return(
        <>
            <StyledProductsList>
                {
                    products ? products.map( (product:any) => {
                        return(
                            <ProductCard key={product.id} cardData={product} setAppData={setAppData} />
                        )
                    } )
                    :
                    <div>No hay productos</div>
                }
                <StyledLoadMoreContainer>
                    <StyledLoadMore onClick={() => setProductsToShow(products.length * 2)}>Load More</StyledLoadMore>
                </StyledLoadMoreContainer>
            </StyledProductsList>
        </>
    )
};

export default ProductListContainer;

// Styles
const StyledProductsList = styled.div`
    align-content: space-around;
    align-items: center;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    padding: 2rem;
    width: 80%;
`;

const StyledLoadMoreContainer = styled.div`
    display: flex;
    justify-content: space-around;
    margin: 2rem 0;
    width: 100%;
`;

const StyledLoadMore = styled.div`
    background-color: #fafafa;
    border: 2px solid black;
    cursor: pointer;
    font-size: 1.2rem;
    font-weight: bold;
    padding: 2rem;
    width: 20%;

    &:hover{
        background-color: #c9c9c9;
        border: 2px solid black;
    }
`;