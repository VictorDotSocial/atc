import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import './App.css';

import ApiService from './services/ApiService';
import StoreContext from './context/StoreContext';

import ProductListContainer from './containers/ProductListContainer';
import CartContainer from './containers/CartContainer';

function App() {
  const [appData, setAppData] = useState({ products: [], cart: [], cartTotal: 0 });
  const [productsToShow, setProductsToShow] = useState(20);
  const [showCart, setShowCart] = useState(false);

  const getStoreData = async(productMaxNumber:number) => {
    const newData = await ApiService.getProducts(productMaxNumber);
    return newData;
  };

  useEffect(() => {
    const updateData = async() => {
      const newData = await getStoreData(productsToShow);
      setAppData({...appData, products: newData});
    }
    updateData();
  }, [productsToShow]);

  return (
    <div className="App">
        <StoreContext.Provider value={appData}>
        <MainContainer>
          <MobileHeader>View Cart</MobileHeader>
          <MainSections>
            <ProductListContainer setAppData={setAppData} setProductsToShow={setProductsToShow} />
            <CartContainer setAppData={setAppData} showCart={showCart} setShowCart={setShowCart} />
          </MainSections>
        </MainContainer>
      </StoreContext.Provider>
    </div>
  );
}

export default App;

// Styles
const MainContainer = styled.div`
  width: 100%;
`;

const MobileHeader = styled.div`
  display: none;

  @media (max-width: 768px) {
    background-color: #fafafa;
    border: 2px solid black;
    cursor: pointer;
    display: block;
    font-size: 1.2rem;
    font-weight: bold;
    margin: 2rem auto;
    padding: 2rem;
    width: 60%;

    &:hover{
        background-color: #c9c9c9;
        border: 2px solid black;
    }
  }
`;

const MainSections = styled.div`
  display: flex;
`;