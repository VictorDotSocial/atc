import React, { useContext } from 'react';
import styled from 'styled-components';

import StoreContext from '../../context/StoreContext';
import objectFinder from '../../utils/objectFinder';

interface CardData {
    cardData: {
        id:string,
        image_url:string,
        stock:number,
        productName:string,
        price:number,
        productDescription:string,
        favorite:string
    },
    setAppData:Function
}

const ProductCard = ({cardData, setAppData}:CardData) => {
    const { image_url, stock, productName, price, productDescription, favorite } = cardData;
    const appData:any = useContext(StoreContext);

    const buttonDisabled:boolean = stock === 0 ? true : false;

    const addButtonHandler = (item) => {
        const newCart = [...appData.cart];
        const itemAlreadyInCartIndex = objectFinder(appData.cart, 'id', item.id);

        if(itemAlreadyInCartIndex !== -1) {
            if(newCart[itemAlreadyInCartIndex].amount === newCart[itemAlreadyInCartIndex].stock) {
                return;
            }
            newCart[itemAlreadyInCartIndex].amount += 1;
        } else {
            item.amount = 1;
            newCart.push(item);
        }

        const newCartTotal = appData.cartTotal + item.price;
        setAppData({...appData, cart: newCart, cartTotal: newCartTotal});
    };

    return (
        <StyledProductContainer>
            <StyledProductHeader image={image_url} favorite={favorite} />
            <StyledProductBody>
                <StyledProductNameDesc>
                    <StyledProductNameTitle>
                        {productName}
                    </StyledProductNameTitle>
                    <StyledProductDescription>
                        {productDescription.length > 120 ? productDescription.slice(0, 120) + '...' : productDescription}
                    </StyledProductDescription>
                </StyledProductNameDesc>
                <StyledProductPrice>
                    {price}€
                </StyledProductPrice>
            </StyledProductBody>
            <StyledProductFooter>
                <div>{stock} left</div>
                <StyledAddButton onClick={() => addButtonHandler(cardData)} disabled={buttonDisabled}>+ add</StyledAddButton>
            </StyledProductFooter>  
        </StyledProductContainer>
    )
};

export default ProductCard;

// Styles
const desktopStyles = {

};

const mobileStyles = {
    ...desktopStyles,
    
};

const StyledProductContainer = styled.div`
    background-color: #f0f0f0;
    border-radius: 10px;
    display: flex;
    flex-direction: column;
    height: 300px;
    margin: 1rem;
    overflow: hidden;
    width: 300px;
`;

const heartFavorite = `
    &:after {
        background-color: #fffd93;
        border-radius: 50%;
        content: '💜';
        left: 45%;
        padding: 0.5rem;
        position: relative;
        top: 5%;
    }
`;

const StyledProductHeader = styled.div`
    background: #000 url('${props => props.image}') no-repeat center center;
    height: 30%;
    overflow: hidden;
    width: 100%;

    ${props => props.favorite === '1' ? heartFavorite : null}
`;

const StyledProductBody = styled.div`
    display: flex;
    height: 50%;
    padding: 0.8rem;
`;

const StyledProductNameDesc = styled.div`
    height: 100%;
    text-align: left;
    width: 80%;
`;

const StyledProductNameTitle = styled.div`
    font-size: 1.2rem;
    font-weight: bold;
`;

const StyledProductDescription = styled.div`
    font-size: 0.9rem;
    font-weight: normal;
    text-overflow: ellipsis;
`;

const StyledProductPrice = styled.div`
    font-size: 1.5rem;
    font-weight: bold;
    height: 100%;
    text-align: right;
    width: 20%;
`;

const StyledProductFooter = styled.div`
    align-items: center;
    display: flex;
    font-size: 1rem;
    font-weight: bold;
    height: 20%;
    justify-content: space-around;
    width: 100%;
`;

const StyledAddButton = styled.button`
    font-size: 1rem;
    font-weight: bold;
`;