
// Finds an object inside an array based on a property name and its value. Returns item index in case of finding it or -1 if not
function objectFinder(arrToCheck, propertyName, propertyValue) {
    const result = arrToCheck.findIndex( item => item[propertyName] === propertyValue);
    return result;
}

export default objectFinder;