
const ApiService = {
    getProducts: async (productMaxNumber:number = 0) => {
        try {
            const response = await fetch('http://localhost:3000/grocery');
            const responseData = await response.json();
            return responseData.slice(0, productMaxNumber);
        } catch (error) {
            console.error('Error: ', error);
        }
    }
}

export default ApiService;